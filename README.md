# Welcome to sneesil_bot

## Running the project 
Host the server in ``` https ```
options are ```heroku``` or ``` ngrok```
### Connecting Telegram with the server 
#### ngrok
```https://api.telegram.org/bot<your_token>/setWebHook?url=https://<your_ngrok_url>.ngrok.io/```
#### heroku
```https://api.telegram.org/bot<your_token>/setWebHook?url=https://<herokuapp>.heroku.com/``` 
Token can be found in ```settings.py```
### Want a demo
Check out the bot ```@sneesil_bot``` in telegram

### Wants to checkout the calls for the commands ?
Check out the ```https://sneesilbot.herokuapp.com/list/```

### Want to add more features ?
ping me in leomv3@gmail.com