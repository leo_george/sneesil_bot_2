from djangoTelegram.settings import BOT_API_KEY,BOT_URL
import urllib.request as request
import urllib.parse as urlparser
import logging
import json
import random

def check_generatemessage(message):
    """
        The function is used to get a message list for a given command

        Parameters:
            message(string): The command which is the input given by user
        
        Returns:
            list: A list which contains some messages for the same command
    """
    commands={"fat":['Danger! Danger! Danger! Fat people break more chairs.','What is the difference between a skinny person and a fat person? \nThe skinny person is very lean and the fat person is very jell-ous'],
    "stupid":['Girl:So, how many times a day do you shave.\n Man: Well, about 15-20 times every day.\n Girl: My god, are you some kind of crazy.\n Man: No, I’m a barber','It has four legs and it can fly, what is it?\n Two birds. '],
    "dumb":['Why can’t you trust an atom?\n Because they make up literally everything.','How do fish get high? Seaweed.'],
    }
    return commands.get(message,None)

def preparemessage(chat_id,message):
    """
        The function is generate a dictionary which contains the chat_id,text,markup keyboard

        Parameters:
            chat_id(string): The chat id of a chat
            message(string): The command given by user
        
        Returns:
            dictionary: A dictionary which contains the parameters required for telegram api
        """
    reply=check_generatemessage(message)
    if reply!=None:
        params={
            'chat_id':chat_id,
            'text':reply[random.choice(range(0,len(reply)))],
            'reply_markup':json.dumps({"keyboard":[['fat'],['stupid'],['dumb']]})
        }
    else:
        params={
            'chat_id':chat_id,
            'text':"Welcome to @sneesil_bot \n Choose one from below:",
            'reply_markup':json.dumps({"keyboard":[['fat'],['stupid'],['dumb']]})
        }
    return params

def sendmessage(params):
    """
        The function is used send a request to telegram api

        Parameters:
            params(dict object): This dictionary is used to send request to telegram api
        
        Returns:
            json: A json which contains the response of the telegram api
    """
    BOT = '%s%s/' % (BOT_URL,BOT_API_KEY)
    message_url =BOT + 'sendMessage'
    req_string=urlparser.urlencode(params).encode()
    req=request.Request(message_url,data=req_string)
    response=request.urlopen(req)
    result=json.loads(response.read().decode())
    return result