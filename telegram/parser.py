class Parser():
    """
    This static class is used to parse the required infomations from the given json  data
    """
    @staticmethod
    def getmessge(json):
        """
        The function is used to get the message string from json data

        Parameters:
            json(json object): The json to be parsed
        
        Returns:
            string: A string which contains the message
        """
        return json['message']['text']
    @staticmethod
    def getchatid(json):
        """
        The function is used to get the chat id from json data

        Parameters:
            json(json object): The json to be parsed
        
        Returns:
            string: A string which contains the chat id
        """
        return json['message']['chat']['id']
    @staticmethod
    def getusername(json):
        """
        The function is used to get the username from json data

        Parameters:
            json(json object): The json to be parsed
        
        Returns:
            string: A string which contains the username
        """
        return json['message']['chat']['username']