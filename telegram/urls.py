from django.urls import path
import telegram.views as views 
urlpatterns = [
    path('',views.index,name="index"),
    path('list/',views.user_details,name='user_details')
]
