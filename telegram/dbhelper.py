from telegram.models import Buttons
from telegram.models import UserDetails

from django.db.models import Count,Avg,Q
import logging

logging.basicConfig(level=logging.INFO)

class ButtonsHelper:
    """
    This static class is used as a helper class for Buttons ORM
    """
    @staticmethod
    def update_button_count(b_name):
        """
        The function is used to update the button count

        Parameters:
            b_name(string): The command which is the button pressed by user
        
        Returns:
            Button : A button object which is updated or created on function invocation
        """
        try:
            button=Buttons.objects.get(pk=b_name)
            button.no_of_calls+=1
            button.save()
        except Buttons.DoesNotExist:
            button=Buttons(button_name=b_name,no_of_calls=0).save()
        return button
    
    @staticmethod
    def get_button_count(b_name):
        """
        The function is used to get a specific button count

        Parameters:
            b_name(string): The command which is the button pressed by user
        
        Returns:
            integer: An Integer which is the no of calls for the given button
        """
        try:
            no_of_calls=Buttons.objects.get(button_name=b_name).no_of_calls
        except Buttons.DoesNotExist:
            no_of_calls=0
        logging.info("get_button_count: %s " % (no_of_calls))
        return no_of_calls

class UserDetailsHelper:
    """
    This static class is used as a helper class for UserDetails ORM
    """
    @staticmethod
    def get_user_list():
        """
        The function is used to get the user list and their command counts

        Returns:
            QuerySet: A QuerySet which contains the distinct usernames and their command counts
        """
        user_list= UserDetails.objects.values('user_name').annotate(fat=Count('button_name',
                filter=Q(button_name=Buttons.objects.get(button_name='fat')))).annotate(dumb=Count('button_name',
                filter=Q(button_name=Buttons.objects.get(button_name='dumb')))).annotate(stupid=Count('button_name',
                filter=Q(button_name=Buttons.objects.get(button_name='stupid'))))
        return user_list

    @staticmethod
    def create_user_detail(chat_id,username,button_obj):
        """
        The function is used to create UserDetails object
        
        Parameters:
            chat_id(string): The chat id of each chat between the users
            username(string): The username of the user sending the request
            button_obj(Buttons): This button which is pressed by the user

        """
        UserDetails(chat_id=chat_id,user_name=username,button_name=button_obj).save()

    @staticmethod
    def get_total_on_critera(button_obj):
        """
        The function is used to get total users on given criteria
        
        Parameters:
            button_obj(Buttons): The button is given as a criteria

        Returns:
            integer: An integer which gives the total no of users on given button criteria

    """
        total=len(UserDetails.objects.filter(button_name=button_obj))
        return total
    