from django.db import models

# Create your models here.
class Buttons(models.Model):
    button_name=models.CharField(max_length=255,primary_key=True)
    no_of_calls=models.BigIntegerField()

#Bonus part
class UserDetails(models.Model):
    chat_id=models.BigIntegerField()
    user_name=models.CharField(max_length=20)
    button_name=models.ForeignKey(Buttons,on_delete=models.CASCADE,db_constraint=False)

