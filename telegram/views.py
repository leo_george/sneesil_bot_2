from django.shortcuts import render
from django.http import HttpResponse,HttpResponseForbidden
import json
import logging
import urllib
from telegram import messenger 
from telegram.parser import Parser
from telegram.models import Buttons
from telegram.dbhelper import ButtonsHelper,UserDetailsHelper
from djangoTelegram.settings import BOT_API_KEY,BOT_URL
# Create your views here
logging.basicConfig(level=logging.INFO)

def index(request):
    """
        The function based view is used get the request from Telegram api and give a response to the same

        Parameters:
            request(WSGI List): contains everything from the request as an element in the list
        
        Returns:
            HttpResponse: A Http response which contains a json shows that the exchanges are done
            HttpResponseForbidden: A Http response saying the saying the request method not possible
    """
    if request.method == "POST":
        data=json.loads( request.body.decode("utf-8"))
        chat_id=Parser.getchatid(data)
        command=Parser.getmessge(data)
        username=Parser.getusername(data)
        if messenger.check_generatemessage(command) is not None:
            ButtonsHelper.update_button_count(command)
            button_name=Buttons.objects.get(button_name=command)
            UserDetailsHelper.create_user_detail(chat_id,username,button_name)
        logging.info("Telegram Request -->  Username:{}  Message:{} ".format(username,command))
        params=messenger.preparemessage(chat_id,command)
        result=messenger.sendmessage(params)
        logging.info("sneesil_bot Response --> Username:{} Message:{}".format(result['result']['chat']['username'],result['result']['text']))
        return HttpResponse(content=json.dumps({"status":"done"}),content_type="application/json")
    else:
        return HttpResponseForbidden(content=json.dumps({"status":"This request method is forbidden"}),content_type="application/json")

def user_details(request):
    """
        The function based is used get the request from users and give a response to the same

        Parameters:
            request(WSGI List): contains everything from the request as an element in the list
        
        Returns:
            HttpResponse: A Http response which contains a json shows that the exchanges are done
            HttpResponseForbidden: A Http response saying the saying the request method not possible
    """
    if request.method=="GET":
        users_list=UserDetailsHelper.get_user_list()
        return render(request,'telegram/list.html',{'users_list':users_list})
    else:
        return HttpResponseForbidden(content=json.dumps({"status":"This request method is forbidden"}),content_type="application/json")
